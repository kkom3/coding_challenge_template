#include<iostream>
using namespace std;
 
int main(){
  int N,A,B;
  
  cin >> N >> A >> B;
 
  
  int sum = 0;
  for (int i=1;i<=N;++i){
    int tsum = i / 10000;
    tsum = tsum + i % 10000 / 1000;
    tsum = tsum + i % 1000 / 100;
    tsum = tsum + i % 100 / 10;
    tsum = tsum + i % 10 / 1;
    if ( A <= tsum && tsum <= B) sum = sum + i; 
  }
  cout << sum <<endl;
  return 0;
}
