#include<bits/stdc++.h> 
#include<vector> 
#include<iterator> 
#include<deque> 
#include<algorithm>
using namespace std;
#define rep(i,n) for(int i = 0; i <(n) ; i++)
#define rep2(i,s,n) for(long long i = (s); i < (n) ; i++)
#define ll long long
#define INF 9999999999;


int main() {
  int N, K;
  cin >> N >> K;
  vector<ll> v(N, 0);
  vector<int> c(N);
  rep(i, N) cin >> c[i];

  rep2(i, 1, N) {
    ll v_temp = INF;
    rep2(j, 1, K + 1) {
      int s = i - j;
      if (s < 0) {
        s = i - 1;
      }
      ll cost = abs(c[i] - c[s]);
      if (v_temp > v[s] + cost) v_temp = v[s] + cost;
    }
    v[i] = v_temp;
    //rep(j,N) cout << v[j] <<" ";
    //cout << endl;
  }
  cout << v[N - 1] << endl;
  return 0;
}
