#include<bits/stdc++.h> 
#include<vector> 
#include<iterator> 
#include<deque> 
#include<algorithm>
using namespace std;
#define rep(i,n) for(int i = 0; i <(n) ; i++)
#define rep2(i,s,n) for(long long i = (s); i(n) ; i++)
#define ll long long

int main(){

  int N,W;
  cin >> N >> W;
  int dp[100][100000]={};

  rep(i,N+1){
    int w,v;
    cin >> w >> v;
    rep(j,W+1){
      int dw;
      if(j-w>0) dw = j-w;
      else dw = w;
      dp[i+1][j] = max(dp[i][w],dp[i][dw]+v);
    }

  }

  cout << dp[N][W] << endl; 


}


