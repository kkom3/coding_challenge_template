#include<iostream>
using namespace std;

void incr(int *v){
  cout << *v << endl;
  cout << v <<endl;
  cout << (*v)++ <<endl;
  cout << *v <<endl;
}

int main(){
  int v = 0;
  incr(&v);
  return 0;
}
