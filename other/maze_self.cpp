#include<bits/stdc++.h>
#include<vector>
#include<iterator>
#include<queue>
#include<algorithm>

#define rep(i,n) for(int i=0; i<(n) ; i++)
typedef long long ll;
typedef pair<int,int> P;

using namespace std;
int main(){
  int H,W,cost[20][20]={};
  char maze[20][20];
  cin >> H >> W;

  rep(i,H) rep(j,W){
    cin >> maze[i][j];
  }

  rep(i,H) rep(j,W){
    if(maze[i][j] == '#') continue;
    int c[20][20] = {};
    int visited[20][20] = {};
    queue<P> q;
    c[i][k] = 0;
    visited[i][j] = 1;

    while(!q.empty()){
      P now = q.front();
      int x = now.first;
      int y = now.second;

      if(x-1 >= 0){
        if(maze[x-1][y] == '.' && visited[x-1][y] == 0){
          c[x-1][y] = d[x][y] + 1;
          q.push(P(x-1,y));
          visited[x-1][y] = 1;
        }
      }

      if(x+1 < H){
        if(maze[x+1][y] == '.' && visited[x+1][y] == 0){
          c[x+1][y] = d[x][y] + 1;
          q.push(P(x+1,y));
          visited[x+1][y] = 1;
        }
      }

      if(y-1 >= 0){
        if(maze[x][y-1] == '.' && visited[x][y-1] == 0){
          c[x][y-1] = d[x][y] + 1;
          q.push(P(x,y-1));
          visited[x][y-1] = 1;
        }
      }

      if(y+1 < W){
        if(maze[x][y+1] == '.' && visited[x][y+1] == 0){
          c[x][y+1] = d[x][y+1] + 1;
          q.push(P(x,y+1));
          visited[x][y+1] = 1;
        }
      }

      q.pop();
    }

    rep(a,H) rep(b,W){
      cost[i][j] = max(cost[i][j],c[a][b]);
    }
  
  }
  int ans = 0;
  rep(i,H) rep(j,W){
    ans = max(cost[i][j],ans);
  }
  cout << ans <<endl;
}



