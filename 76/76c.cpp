#include<bits/stdc++.h>
#include<random>
#include<algorithm>
#include<unistd.h>
#define rep(i,n) for(int i = 0 ; i < (n) ; i++)
using namespace std;
 

int main()
{

  string Sin;
  string T;
  cin >> Sin;
  cin >> T;

  int start = -1;
  vector<int> sv;

  //cout << T.size() << endl;
  rep(i,Sin.size()-T.size()+1){
    start = i;
    rep(j,T.size()){
      //cout << Sin[i+j] << " " << T[j] << endl;
      if(Sin[i+j] != T[j] && Sin[i+j] != '?')break;
      if(j==T.size()-1) sv.push_back(start);
    }
  }

  if (sv.size() == 0){
    cout << "UNRESTORABLE" << endl;
    return 0;
  }
  int count = 0;
  rep(i,int(Sin.size())){
    if(sv.at(sv.size() - 1) <= i && i<sv.at(sv.size() - 1) + T.size()){
      cout << T[count];
      //cout << sv.at(sv.size()-1) << " " << sv.size() << endl;
      ++count;
    }
    else if(Sin[i] == '?')cout <<'a';
    else cout<<Sin[i];
    
  }
  cout << endl;
  return 0;
}
