#include<bits/stdc++.h>
#include<random>
#include<algorithm>
#define rep(i,n) for(int i = 0 ; i < (n) ; i++)
using namespace std;

#define MOD 1000000007


long long modinv(long long a, long long m){
  long long b = m, u = 1, v = 0;
  while(b){
    long long t = a/b;
    a -= t * b; swap(a,b);
    u -= t * v; swap(u,v);
  }
  u %= m;
  if (u<0) u += m;
  return u;
}

long long COM(int n,int k,long long before){
  
  long long u = (n - k + 1) % MOD;
  long long s = modinv(k,MOD);
  long long now = (before * u * s) %  MOD;

}




int main(){
  int n,a,b;
  cin >> n >> a>> b;
  
  long long sum = 1;
  long long temp = 1;

  for(int i = 1;i<n;i++){
    temp =  COM(n,i,temp);
    if(i != a && i != b){
      sum = (sum + temp)%MOD;
    }
  }
  cout << sum << endl;
  return 0;
    
  
}
