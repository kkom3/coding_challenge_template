#include<bits/stdc++.h>
#include<random>
#include<algorithm>
#include<math.h>
#define rep(i,n) for(int i = 0 ; i < (n) ; i++)
using namespace std;
 
int main(){
  int N;
  cin >> N;
  vector<int> v;
  rep(i,N){
    int temp;
    cin>>temp;
    v.push_back(temp);
  }


  sort(v.begin(),v.end());

  vector<long long> r;
  for(int i=v.at(0);i<=v.at(v.size()-1);i++){
    int sum = 0;
    rep(j,N){
      sum = sum + (v.at(j) - i) * (v.at(j)-i);
    }
    r.push_back(sum);
  }

  sort(r.begin(),r.end());
  cout<< r.at(0) <<endl;

  return 0;
}
