#include<bits/stdc++.h>
#include<random>
#include<algorithm>
#define rep(i,n) for(int i = 0 ; i < (n) ; i++)
using namespace std;
 
 
int main(){
  int N;
  cin >> N;
 
  map<string,int> s;
 
  rep(i,N){
    string temp;
    cin >> temp;
    auto result = s.find(temp);
    if(result == s.end()){
      s.insert(make_pair(temp,0));
    }
    else{
      s[temp] = s[temp] + 1;
    }
  }
  
  multimap<int,string,greater<int>> s2;
  for(auto i =s.begin(); i != s.end() ; i++) s2.insert(make_pair(i->second,i->first));

  multimap<int,string>::iterator i = s2.begin();
  int temp = i->first;
  vector<string> answer;
  for(; i != s2.end() ; ++i){
    if(i->first != temp) break;
    else answer.push_back(i->second);
  }

  sort(answer.begin(),answer.end());
  rep(i,answer.size()) cout << answer.at(i) << endl;

 
  return 0;
}
