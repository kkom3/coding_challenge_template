#include<bits/stdc++.h>
#include<random>
#include<algorithm>
#define rep(i,n) for(int i = 0 ; i < (n) ; i++)
using namespace std;
 
 
int main(){
  int N;
  cin >> N;
 
  vector<string> s;
  vector<int> num;
 
  rep(i,N){
    string temp;
    cin >> temp;
    auto result = lower_bound(s.begin(),s.end(),temp);
    if(result == s.end()){
      s.push_back(temp);
      num.push_back(1);
    }
    else{
      num.at(result-s.begin()) = num.at(result-s.begin()) + 1;
    }
  }
 
  typedef pair<int,int> pair;
  vector<pair> pair_vec;
 
  rep(i,num.size()){
    pair_vec.push_back(pair(i,num[i]));
  }
  sort(pair_vec.begin(),
       pair_vec.end(),
       [](const pair& x, const pair& y){return x.second < y.second;}
  );
  
  
  //cout << num.size() <<endl;
  //rep(i,num.size()) cout << pair_vec.at(i).first << pair_vec.at(i).second <<endl;
  
  vector<string> answer;
  int point = pair_vec.at(pair_vec.size()-1).second;
  rep(i,num.size()){
    int gap = num.end() - num.begin();
    int index = gap - i - 1;
    int sindex = pair_vec.at(index).first;
    int npoint = pair_vec.at(index).second;
    //cout << point << npoint <<endl;
    if (point != npoint) break;
    answer.push_back(s.at(sindex));
  }
  
  sort(answer.begin(),answer.end());
  //cout << answer.size() <<endl;
  rep(i,answer.size()) cout << answer.at(i) <<endl;
 
  
 
  
 
  return 0;
}